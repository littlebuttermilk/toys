package perctools

import (
	"github.com/gookit/color"
	"golang.org/x/exp/rand" // required for gonum sampling vs math/rand
	"gonum.org/v1/gonum/graph/simple"
	"gonum.org/v1/gonum/stat/distuv"
	"gonum.org/v1/gonum/stat/sampleuv"
	"time"
)

//Percolator, a type for convenience in comparisons with range indices
type Percolator struct {
	graph *simple.UndirectedGraph
	N     int
	Ids   []int
	Spout []int
}

// NewPercolator randomly generates iid bernoulli and construct percolator graph
func NewPercolator(prob float64, N int, seed uint64) Percolator {

	// init
	// fyi not concurrency safe but create new each routine
	src := rand.NewSource(seed)
	nb := []int{}

	Pg := Percolator{
		graph: simple.NewUndirectedGraph(),
		N:     N,
	}

	b := distuv.Bernoulli{
		P:   prob,
		Src: src,
	}
	bs := sampleuv.IIDer{
		Dist: b,
	}

	// draw
	bsample := make([]float64, N*N)
	bs.Sample(bsample)

	// make graph
	// in future just better integrate the node info from the graph into rest of prog
	// awakrdness bc if you create node before neighbor range,
	// you need first to check that the node was not already created by SetEdge
	// but if you rely only on setedge to build nodes, isolated ones don't get created
	// leaving mismatch between Ids and actual nodes
	for u, val := range bsample {
		if val == 1 {
			Pg.Ids = append(Pg.Ids, u)
			nb = neighbor(u, bsample, N)
			if len(nb) == 0 {
				Pg.graph.AddNode(simple.Node(u))
			}
			for _, v := range nb {
				Pg.graph.SetEdge(simple.Edge{F: simple.Node(u), T: simple.Node(v)})
			}
		}
	}

	return Pg
}

// Print plays the percolator game in terminal
func (Pg Percolator) Print(s string, d int) {
	base := make([][]string, Pg.N)
	result := make([][]string, Pg.N)
	ids := idConvert(Pg.Ids, Pg.N)
	spt := idConvert(Pg.Spout, Pg.N)

	// two containers for dramatic finish
	// initialize with blanks for nice printing
	for i := range base {
		base[i] = make([]string, Pg.N)
		reset(base[i])
		result[i] = make([]string, Pg.N)
		reset(result[i])
	}

	// fill with node placement
	for _, v := range ids {
		base[v[0]][v[1]] = s
	}

	// fill with spout if any
	for _, v := range spt {
		result[v[0]][v[1]] = s
	}

	// print percolator rowwise
	for _, r := range base {
		color.BgCyan.Println(r)
	}

	// sleep so player can look for path visually
	time.Sleep(time.Duration(d) * time.Second)

	// flow animation for result
	if len(spt) == 0 {
		color.Style{color.FgCyan, color.OpBold}.Println("{-_-}")
	} else {
		for _, v := range result {
			color.Style{color.FgCyan, color.OpBold}.Println(v)
			time.Sleep(100 * time.Millisecond)
		}
	}

}

// convert long-form Ids and Spout to wide-form for printing
func idConvert(ids []int, N int) map[int][2]int {
	idmap := make(map[int][2]int)
	for _, v := range ids {
		idmap[v] = [2]int{int(v / N), v % N}
	}
	return idmap
}

// reset row of percolator plot to blank
// for dramatic finish in Print
func reset(r []string) {
	for i := range r {
		r[i] = " "
	}
}

// returns neighbor set of node id u
// ie bsample[u] == 1
// this should be smoothed out a bit
// will lead to duplication since this will iterate over bsample[u]
func neighbor(u int, b []float64, N int) (nb []int) {
	// to identify boundary nodes at 0, N-1
	boundary := u % N

	for i, v := range b {
		if v == 1 {
			// extra conditions so that N, N+1 not connected
			switch e := abs(u - i); e {
			case N:
				nb = append(nb, i)
			case 1:
				if boundary == 0 && u-i < 0 {
					nb = append(nb, i)
				} else if boundary == N-1 && u-i > 0 {
					nb = append(nb, i)
				} else if boundary > 0 && boundary < N-1 {
					nb = append(nb, i)
				}
			default:
				continue
			}
		}
	}

	return
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
