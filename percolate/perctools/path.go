package perctools

import (
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/path"
)

/*Downspout uses dijkstra to find a path from top to bottom
dijkstra is way overkill. if you didn't care about the whole path,
could use From method on bottom
return first path found from top to bottom*/
func (Pg *Percolator) DownSpout() {
	top := topRow(Pg.Ids, Pg.N)
	bottom := bottomRow(Pg.Ids, Pg.N)
	p := []graph.Node{}

	for _, u := range top {

		pth := path.DijkstraFrom(Pg.graph.Node(int64(u)), Pg.graph)

		// surely there is a better way
		for _, v := range bottom {
			p, _ = pth.To(int64(v))

			if len(p) > 0 {
				for _, i := range p {
					Pg.Spout = append(Pg.Spout, int(i.ID()))
				}
				return
			}
		}
	}

}

func bottomRow(ids []int, N int) (bottom []int) {
	for _, u := range ids {
		if int(u/N) == N-1 {
			bottom = append(bottom, u)
		}
	}

	return
}

func topRow(ids []int, N int) (top []int) {
	for _, u := range ids {
		if int(u/N) == 0 {
			top = append(top, u)
		}
	}

	return
}
