module gitlab.com/littlebuttermilk/toys/percolate

go 1.13

require (
	github.com/gookit/color v1.3.6
	golang.org/x/exp v0.0.0-20210201131500-d352d2db2ceb
	gonum.org/v1/gonum v0.8.2
)
