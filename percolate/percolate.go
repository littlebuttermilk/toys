package main

import (
	"flag"
	"gitlab.com/littlebuttermilk/toys/percolate/perctools"
	"os"
	"time"
)

func main() {
	var prob float64
	var N int
	var d int
	seed := uint64(time.Now().Nanosecond())

	flag.Float64Var(&prob, "prob", 0.5, "probability of bond, required in [0, 1]")
	flag.IntVar(&N, "N", 10, "square grid side length, required >= 1")
	flag.IntVar(&d, "delay", 5, "delay in sec given before solution displayed, required >= 1")
	flag.Parse()

	if prob < 0 || prob > 1 || N < 1 || d < 1 {
		flag.Usage()
		os.Exit(2)
	}

	Pg := perctools.NewPercolator(prob, N, seed)
	Pg.DownSpout()
	Pg.Print("*", d)
}
