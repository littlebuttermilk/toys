Sometimes I just want to look at a [percolation](https://en.wikipedia.org/wiki/Percolation_theory) in my terminal.

**Usage:**

`percolate [-N <value>] [-prob <value>] [-delay <value>]`

- generate and print 2d percolation model on `N` by `N` grid
- try to find a path from top to bottom in less than `delay` seconds!
- print solution if any

right now just an excuse to play with gonum/graph, which leads to some awkwardness. uses dijkstra to find a crossing path, but this is somewhat overkill.

next up might be to run concurrently random walks with downward drift on the graph to find the path, or a current-reinforced walk with shared information across routines --- somewhat like ['anytime' MC algorithms](https://xianblog.wordpress.com/tag/abc/)
