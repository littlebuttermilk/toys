package main

import (
	"flag"
	"github.com/gookit/color"
	"math/rand"
	"time"
)

func main() {
	var n int
	var seed int64
	flag.IntVar(&n, "n", 90, "n steps to walk")
	flag.Int64Var(&seed, "seed", int64(time.Now().Nanosecond()), "rng seed")
	flag.Parse()

	s := make([][]string, n)
	w := 0
	var p float64

	rand.Seed(seed)

	for v := range s {
		s[v] = make([]string, 30)
		p = rand.Float64()
		if w == 29 {
			w -= 1
		} else if w == 0 {
			w += 1
		} else {
			if p < 0.5 {
				w += 1
			} else {
				w -= 1
			}
		}
		s[v][w] = "-"
		color.FgRed.Println(s[v])
	}
}
